#!/usr/bin/env python
import argparse
import sys

def parse_command_line():
  parser = argparse.ArgumentParser('Assignment 2')
  parser.add_argument('dimension', type=valid_dimension,
                       default=11,
                       help='Dimension of the table output')
  return parser.parse_args()

def valid_dimension(string):
  value = int(string)
  if value < 1 or value > 200:
    msg = "%r should be posative and less than 200" % string
    raise argparse.ArgumentTypeError(msg)
  return value

def draw(matrix):
  for i in matrix:
    for j in i:
      if j:
        sys.stdout.write('|x')
      else:
        sys.stdout.write('|.')
    print('|')

def make_an_x_shape(dimension):
  matrix = [[False] * dimension for i in range(dimension)]
  i = 0
  j = 0
  while i < dimension:
    matrix[i][j] = True
    matrix[dimension-1-i][j] = True
    i += 1
    j += 1
  return matrix

def main():
  args = parse_command_line()
  matrix = make_an_x_shape(args.dimension)
  draw(matrix)

if __name__ == '__main__':
  main()
